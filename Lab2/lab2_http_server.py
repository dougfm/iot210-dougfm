#!/usr/bin/python
# =============================================================================
#        File : http_server.py
# Description : Lab2 HW 
#      Author : Doug Muehlbauer 
#        Date : 1/16/2018 
# =============================================================================
import random
import string
import json
import sys

PORT = 5000

from flask import Flask, request

# ============================== APIs ====================================

# create the global objects
app = Flask(__name__)

@app.route("/pi/temp", methods=['GET'])
def GETTemp():

  rsp_data = "Echo:\nMethod: "

  if request.method == 'GET':

    rsp_data += "GET\n"

    from sense_hat import SenseHat
    sense = SenseHat()

    temp = sense.get_temperature()
    temp = round(temp, 1)
    if temp > 29.0:
      # sense hat is getting hot, show temp in RED
      sense.show_message(str(temp), text_colour=[255,0,0])
    else:
      # sense hat temp is normal, show temp in GREEN
      sense.show_message(str(temp), text_colour=[0,255,0])

    rsp_data += "Current temperature = " + str(temp)
    return rsp_data, 200


@app.route("/pi/input", methods=['PUT'])
def PUTText():
  print("request.data = " + request.data)
  
  rsp_data = request.data 
  return rsp_data, 200


@app.route("/pi/home")
def Home():
 
  rsp_header=""
  print ("-------Start header values------------------------------------")
  for h_key, h_val in request.headers:
    rsp_header += "Key: " + str(h_key) + "=" + str(h_val)
    print ("Key: " + str(h_key) + "=" + str(h_val))
  
  print ("-------End header values------------------------------------")
  return rsp_header, 200


# ============================== Main ====================================

if __name__ == "__main__":

  print "HTTP Server"

  app.debug = True
  app.run(host='0.0.0.0', port=PORT)
